import os
import glob
from tqdm import tqdm
import json
from typing import List, Tuple, Dict
import cv2
import numpy as np
from skimage.measure import label, regionprops
from skimage import measure
from skimage import draw

from sklearn.model_selection import LeaveOneOut
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import train_test_split
import copy
from shapely.geometry import Polygon, MultiPolygon

from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.utils.visualizer import ColorMode
from detectron2.utils.visualizer import Visualizer
import random
from detectron2.structures import BoxMode
import pickle
import math

def get_pancreas_labels():
    """Load the mapping that associates pascal classes with label colors
    Returns:
        np.ndarray with dimensions (21, 3)
    """
    return np.asarray([[249, 179, 110], #pancreas_1
                       [209, 151, 93],  #pancreas_2
                       [147, 105, 66],  #pancreas_3
                       [216, 131, 105]  #stomach
                       ])

def get_ME_labels():
    """Load the mapping that associates pascal classes with label colors
    Returns:
        np.ndarray with dimensions (21, 3)
    """
    return np.asarray([[38,121,26], # dissection plane TME
                       [255,0,0],  # dissection line TME
                       [160,130,112],  #rectum
                       [244,172,147] # seminal vesicles
                       ])

def get_VD_labels():
    """Load the mapping that associates pascal classes with label colors
    Returns:
        np.ndarray with dimensions (21, 3)
    """
    return np.asarray([[216,100,79],
                       [0,151,205],
                       [184,232,61],
                       [230,168,28]
                       ])

def get_MM_labels():
    """Load the mapping that associates pascal classes with label colors
    Returns:
        np.ndarray with dimensions (21, 3)
    """
    return np.asarray([[151,188,207], # gerotas fascia
                       [128,174,128],  # mesocolon
                       [219,244,20],  # dissection line gerota
                       [255,61,193] # exploration area
                       ])

def get_first_labels():
    """Load the mapping that associates pascal classes with label colors
    Returns:
        np.ndarray with dimensions (21, 3)
    """
   # return #np.asarray([[230,0,205], # first incision
           #            [128,77,0],  # plane first incesion
           #            [255,255,0],  # incised area
           #            [0,0,126], #instrument
           #            [96,66,54], # instrument
           #            #[240, 214, 144],  # instrument
           #            [255,255,255],#compress
           #            [170,255,255],
           #            [84,84,126],# instrument
           #            [84, 84, 255],  # instrument
           #            [0,0,0]# instrument
           #            ])

    return np.asarray([[128,77,0],
                       [230,0,205],
                       [255,255,0],
                       [0,0,126],
                       [96,66,54],
                       [0,174,4],
                       [84,84,126],
                       [255, 255, 255],
                       [170, 255, 255]
                       ])

def get_LM_labels():
    """Load the mapping that associates pascal classes with label colors
    Returns:
        np.ndarray with dimensions (21, 3)
    """
    return np.asarray([[170, 84, 68], #abdominal wall
                       [204, 168, 142],  #colon
                       [110, 184, 209],  #adhesion
                       [230, 219, 70], #fat
                       [255, 207, 177]#small intensine
                       ])

def get_Nerves_labels():
    return np.asarray([[230, 219, 70],  #pd
                       [216, 100, 79],  # artery
                       [0, 151, 205]  # vein
                       ])

def get_PD_labels():
    return np.asarray([[230, 219, 70],  #nerve
                       [216, 100, 79]  # artery

                       ])

def get_Nerves_label_desc():
    return np.asarray(["nerve",
                       "artery",
                       "vein"
                       ])

def get_pancreas_labels_for_vis():
    """Load the mapping that associates pascal classes with label colors
    Returns:
        np.ndarray with dimensions (21, 3)
    """
    return np.array(
    [(249, 179, 110), #pancreas_1
      (209, 151, 93),  #pancreas_2
       (147, 105, 66),  #pancreas_3
        (216, 131, 105)  #stomach
                       ])

def get_pancreas_label_desc():
    """Load the mapping that associates pascal classes with label colors
    Returns:
        np.ndarray with dimensions (21, 3)
    """
    return np.asarray(["pancreas_1",
                       "pancreas_2",
                       "pancreas_3",
                       "stomach"
                       ])

def get_PD_label_desc():
    return np.asarray(["pedicle package",
                       "inferior mesenteric artery"
                       ])

def get_LM_label_desc():
    return np.asarray(["abdominal wall",
                       "colon",
                       "adhesion",
                       "fat",
                       "small intensine"
                       ])

def get_MM_label_desc():
    return np.asarray(["gerotas fascia",
                       "mesocolon",
                       "dissection line gerota",
                       "exploration area"

                       ])

def get_first_label_desc():
    #return np.asarray(["first incision", "plane first incision", "incised area", "instrument fenestrated bipolar forceps", "instrument permanent cautery hook", "instrument cadiere forceps","compress","background","instrument cadiere forceps","instrument assistant","instrument monopolar curved scissors"])

    return np.asarray([
 "plane first incision",
    "first incision",
 "incised area",
 "instrument fenestrated bipolar forceps",
 "instrument permanent cautery hook",
"instrument monopolar curved scissors",
 "instrument cadiere forceps",
 "compress",
    "background"])

def get_ME_label_desc():
    return np.asarray(["dissection plane TME",
                       "dissection line TME",
                       "rectum",
                       "seminal vesicles"
                       ])

def get_VD_label_desc():
    return np.asarray(["inferior mesenteric artery",
                       "inferior mesenteric vein",
                       "plastic clip",
                       "titan clip "
                       ])

class CreateDescrition():

    def __init__(self,
                 img_base_path,
                 sub_dirs,
                 purpose,
                 merge_masks,
                 dataset_dict_file_name,
                 output_base_path,
                 dataset):

        self.img_base_path=img_base_path
        self.sub_dirs=sub_dirs
        self.purpose=purpose
        self.merge_masks=merge_masks
        self.dataset_dict_file_name=dataset_dict_file_name
        self.output_base_path=output_base_path
        #self.checkup_path = checkup_path
        self.dataset = dataset


    def distance(self,a, b):
        x = a[0] - b[0]
        y = a[1] - b[1]

        return math.sqrt(x * x + y * y)

    def components(self,image, cl, c_id, merge):
        cl_map = image#(image == cl).astype(np.uint8)
        components = np.zeros(cl_map.shape, dtype=np.uint8)
        # test = np.zeros(cl_map.shape, dtype=np.uint8)
        cur_comp = 1
        shapes = [[]]
        for y in range(cl_map.shape[0]):
            for x in range(cl_map.shape[1]):
                queue = []
                if cl_map[(y, x)] != 0 and components[(y, x)] == 0:
                    components[(y, x)] = cur_comp
                    queue.append((y, x))
                   # print(x, y)
                    while len(queue) > 0:
                        y_o, x_o = queue.pop()

                        inside = 0
                        for x_n in range(-1, 2):
                            for y_n in range(-1, 2):
                                x_d = x_o + x_n
                                y_d = y_o + y_n

                                if x_d < 0 or y_d < 0 or x_d >= cl_map.shape[1] or y_d >= cl_map.shape[0]:
                                    inside += 1
                                    continue

                                if cl_map[(y_d, x_d)] != 0 and components[(y_d, x_d)] == 0:
                                    components[(y_d, x_d)] = cur_comp
                                    queue.append((y_d, x_d))
                                elif cl_map[(y_d, x_d)] == 0:
                                    inside += 1
                        if inside > 1:
                            # shapes[-1].append((y_o, x_o))
                            shapes[-1].append((x_o, y_o))
                            # test[(y_o, x_o)] = 255
                    cur_comp += 1
                    shapes.append([])
                #   print(x,y)
        # color = (255, 0, 0)
        objs = []

        for s in shapes:
            if len(s) > 0:
                old_s = s[1:]
                new_s = [s[0]]
                min_x = s[0][0]
                min_y = s[0][1]
                max_x = s[0][0]
                max_y = s[0][1]

                while len(old_s) > 0:
                    best_id = 0
                    best_val = self.distance(new_s[-1], old_s[0])
                    for i in range(1, len(old_s)):
                        d = self.distance(new_s[-1], old_s[i])
                        if d < best_val:
                            best_val = d
                            best_id = i
                    if best_val < 3:
                        new_s.append((old_s[best_id][0], old_s[best_id][1]))
                        #new_s.append(old_s[best_id][0])
                        #new_s.append(old_s[best_id][1])

                        min_x = min(min_x, old_s[best_id][0])
                        min_y = min(min_y, old_s[best_id][1])
                        max_x = max(max_x, old_s[best_id][0])
                        max_y = max(max_y, old_s[best_id][1])
                    old_s.pop(best_id)
                #if len(new_s) < 20 and (max_y - min_y) * (max_x - min_x) < 100:
                #    continue
            if merge and len(objs) != 0:
                box = objs[-1]["bbox"]

                box[0] = min(box[0], min_x)
                box[1] = min(box[1], min_y)
                box[2] = max(box[2], max_x)
                box[3] = max(box[3], max_y)
                objs[-1]["bbox"] = box
                objs[-1]["segmentation"].append(new_s)
            else:

                px = []
                py = []
                poly = []
                for ii in range(len(new_s)):
                    poly.append(new_s[ii][0])
                    poly.append(new_s[ii][1])

                #poly = [(x, y) for x, y in zip(px,py)]
                #poly = [p for x in poly for p in x]

                if len(new_s) > 5:# and (max_y - min_y) * (max_x - min_x) > 100:
                    obj = {
                        "bbox": [min_x, min_y, max_x, max_y],
                        "bbox_mode": BoxMode.XYXY_ABS,
                        "segmentation": [new_s],
                        "category_id": c_id,
                        "iscrowd": 0
                    }
                    objs.append(obj)
                else:
                    print("Segment was too small. ", len(new_s))


        return objs

    def load_dataset_dict(self,dataset_dict_file):

        with open(dataset_dict_file, 'rb') as f:
            dataset_dict = pickle.load(f)

            return dataset_dict

        return None

    def load_dataset_dicts_old(self,json_file):
                        #img_dirs: str,
                          #json_with_desription_name: str =
                          #"dataset_registration_detectron2_multilabeled_all_layers.json") -> List[dict]:
        """
        Creating a description for each image in the image dir according to the json description
        While extracting the description of from the json, Bounding Boxes will be also calculated

        Args:
            img_dir: dir where the images are located
            json_with_desription_name: description json of every single image in the img_dir

        Returns:
            List with description of every image
            F.e
            [{'height': 540, 'width': 960, 'file_name':
            '/instruments/train/frame_00000.png',
             'image_id': 0, 'annotations': [{'bbox': [0.0, 218.12345679012344,
              630.0987654320987, 539.0],
             'bbox_mode': <BoxMode.XYXY_ABS: 0>,
             'segmentation': [[0.5, 521.5, 435.537037037037, 297.6358024691358,
                , 355.2901234567901, 415.537037037037, 125.5, 539.5,
                1.5864197530864197, 538.9938271604938]],
                'category_id': 2}, ..... ]


        """


        print("Load json file: ",json_file)
        #json_file = os.path.join(img_dir, json_with_desription_name)

        #json_file = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/pancreas_val.json"
        #json_file = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/dataset.json"
        with open(json_file) as f:
            imgs_anns = json.load(f)

        #with open(json_file, 'rb') as f:
        #    imgs_anns = pickle.load(f)

        dataset_dicts = []
        for idx, v in enumerate(imgs_anns.values()):
            try:
                record = {}

                filename = v["file_name"]
                record["height"] = v['height']
                record["width"] = v['width']
                record["file_name"] = filename
                record["image_id"] = idx
                annos = v["regions"]
                objs = []
                for _, anno in annos.items():
                    assert not anno["region_attributes"]
                    anno = anno["shape_attributes"]
                    py = anno["all_points_x"]
                    px = anno["all_points_y"]
                    poly = [(x + 0.5, y + 0.5) for x, y in zip(px, py)]
                    poly = [p for x in poly for p in x]

                    obj = {
                        "bbox": [
                            np.min(px),
                            np.min(py),
                            np.max(px),
                            np.max(py)],
                        "bbox_mode": BoxMode.XYXY_ABS,
                        "segmentation": [poly],
                        "category_id": anno['category_id'],
                    }
                    objs.append(obj)
                record["annotations"] = objs
                dataset_dicts.append(record)
            # except:
            except Exception as ex:
                #filename = os.path.join(img_dir, v["file_path"])
                print(f'could not process {filename} {ex}')
        return dataset_dicts


    def create_json(self,imgs, labels, padding, json_file_path):
        json_dict = dict()

        # iteration over each image
        for ii in tqdm(range(len(imgs))):


            img = cv2.imread(imgs[ii])
            imgs[ii] = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/20200330/images/frame_0021.png"
            img = cv2.imread("/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/20200330/images/frame_0021.png")
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            label = cv2.imread(labels[ii])

            label = cv2.imread("/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/20200330/labels/label_0021.png")
            label = cv2.cvtColor(label, cv2.COLOR_BGR2RGB)


            if (np.max(label) == 0.0):
                print("\nLabel is black!")
                continue

            filename = os.path.basename(imgs[ii])
            single_image = {}
            single_image['file_name'] = imgs[ii]


            single_image['fileref'] = ""
            single_image['size'] = img.size
           # print(filename)
            height, width, channels = img.shape
            single_image['height'] = height
            single_image['width'] = width
            single_image['base64_img_data'] = ""
            single_image['file_attributes'] = {}

            single_image['regions'] = generate_regions(label_mask=label, padding=padding)
            json_dict[imgs[ii]] = single_image

            break


        with open(json_file_path, 'w') as f:
            json.dump(json_dict, f)

        classes_list = ["pancreas_1", "pancreas_2", "pancreas_3", "stomach"]

        for d in ["train"]:
            DatasetCatalog.register("pancreas_" + d, lambda d=d: get_pancreas_dicts(json_file_path))
            MetadataCatalog.get("pancreas_" + d).set(thing_classes=classes_list)

        pancreas_metadata = MetadataCatalog.get("pancreas_train")

        dataset_dicts = get_pancreas_dicts(json_file_path)


        for d in dataset_dicts:

            img = cv2.imread(d["file_name"])
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            visualizer = Visualizer(img[:, :, ::-1], metadata=pancreas_metadata, scale=0.5, instance_mode=ColorMode.SEGMENTATION)
            vis = visualizer.draw_dataset_dict(d)
            cv2.imwrite("/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/Experiments/foo.png",
                    vis.get_image()[:, :, ::-1])

    def create_dataset_description(self):#,img_base_path, sub_dirs, purpose, dataset_dict_file_name, output_base_path, merge_masks):

        imgs = []
        labels = []


        # collect all paths togehter
        for ii in range(len(self.sub_dirs)):
            images = glob.glob(os.path.join(self.img_base_path,self.sub_dirs[ii],"images/")+"*.png") #

            for jj in range(0,len(images)):
                imgs.append(images[jj])
                os.path.basename(images[jj])
                labels.append(os.path.join(self.img_base_path,self.sub_dirs[ii],"labels/")+os.path.basename(images[jj]).replace("frame","label"))#self.img_base_path,


        if self.purpose == "train_val":
            img_train, img_val, label_train, label_val = train_test_split(imgs, labels, test_size = 0.20, random_state = 42)

            self.create_dataset_dict(imgs=img_train,
                        labels=label_train,
                       #padding=padding,
                                merge_masks=self.merge_masks,
                        dataset_dict_file_path=os.path.join(self.output_base_path,self.dataset_dict_file_name+"_train.pkl"),
                                     dataset=self.dataset)

            self.create_dataset_dict(imgs=img_val,
                        labels=label_val,
                                merge_masks=self.merge_masks,
                        #padding=padding,
                        dataset_dict_file_path=os.path.join(self.output_base_path, self.dataset_dict_file_name + "_val.pkl"),
                                     dataset=self.dataset)

        if self.purpose == "test":
            self.create_dataset_dict(imgs=imgs,
                        labels=labels,
                                merge_masks=self.merge_masks,
                        #padding=padding,
                        dataset_dict_file_path=os.path.join(self.output_base_path, self.dataset_dict_file_name + "_test.pkl"),
                                     dataset=self.dataset)

    def encode_segmap(self,mask, label_value):
        """Encode segmentation label images as pascal classes
        Args:
            mask (np.ndarray): raw segmentation label image of dimension
              (M, N, 3), in which the Pascal classes are encoded as colours.
        Returns:
            (np.ndarray): class map with dimensions (M,N), where the value at
            a given location is the integer denoting the class index.
        """
        mask = mask.astype(int)
        label_mask = np.zeros((mask.shape[0], mask.shape[1]), dtype=np.int16)
        #for ii, label in enumerate(get_pascal_labels()):
        label_mask[np.where(np.all(mask == label_value, axis=-1))[:2]] = 1
        label_mask = label_mask.astype(int)
        return label_mask

    def label_to_binary(self,mask):

        mask = mask.astype(int)
        label_mask = np.zeros((mask.shape[0], mask.shape[1]), dtype=np.int16)
        for ii, label in enumerate(get_pancreas_labels()):
            label_mask[np.where(np.all(mask == label, axis=-1))[:2]] = 1
        label_mask = label_mask.astype(int)
        return label_mask

    def encode_segmap(self,mask, label_value):
        """Encode segmentation label images as pascal classes
        Args:
            mask (np.ndarray): raw segmentation label image of dimension
              (M, N, 3), in which the Pascal classes are encoded as colours.
        Returns:
            (np.ndarray): class map with dimensions (M,N), where the value at
            a given location is the integer denoting the class index.
        """
        mask = mask.astype(int)
        label_mask = np.zeros((mask.shape[0], mask.shape[1]), dtype=np.int16)
        #for ii, label in enumerate(get_pascal_labels()):
        label_mask[np.where(np.all(mask == label_value, axis=-1))[:2]] = 1
        label_mask = label_mask.astype(int)
        return label_mask

    def generate_regions(self,label_mask, padding, debug=False):

        regions = {}
        region_index = 0

        #merged_masked = label_to_binary(mask=label_mask)

        #a = np.max(np.where(merged_masked > 0)[0]) - np.min(np.where(merged_masked > 0)[0])
        #b = np.max(np.where(merged_masked > 0)[1]) - np.min(np.where(merged_masked > 0)[1])

        #label_area = a * b
        for ii, label_rgb_val in enumerate(get_pancreas_labels()):
            binary_mask = encode_segmap(mask=label_mask, label_value=label_rgb_val)

            if np.max(binary_mask) > 0: # sonst ist die maske leer

                lbl_0 = label(binary_mask)
                props = regionprops(lbl_0)

                mask_like = np.zeros_like(binary_mask)
                props_amount = len(props)
                #if props_amount > 1:

                if debug:
                    print(f"\nFor label", str(label_rgb_val)," we found ",props_amount)

                # get the layer name
                #layer = os.path.splitext(os.path.basename(single_mask))[0][-2:]

                for index_prop, prop in enumerate(props):

                    if (prop.bbox[0] - padding) < 0 or (prop.bbox[1] - padding) <  0:
                        rectangle1 = binary_mask[prop.bbox[0]:prop.bbox[2],
                                     prop.bbox[1]:prop.bbox[3]]
                    else:
                    #    padding
                        rectangle1 = binary_mask[prop.bbox[0] - padding:prop.bbox[2] + padding, prop.bbox[1] - padding:prop.bbox[3] + padding]
                    # trying to find contours if not possible proceed to next layer
                    try:
                        contours = measure.find_contours(array=rectangle1, level=0.2)#, fully_connected="high", positive_orientation="high", mask=binary_mask)
                    except BaseException:
                        print("could not calculate contour")
                        continue


                    def addition_y(y):
                        return y + prop.bbox[1] - padding

                    def addition_x(x):
                        return x + prop.bbox[0] - padding

                    max_length, index_contour = max(
                        [(i.shape[0], index) for index, i in enumerate(contours)])

                    #for index, i in enumerate(contours)
                    #    print(i.shape[0])

                    for n, contour in enumerate(contours):
                        y = addition_y(contours[index_contour][:, 1].astype(int))
                        x = addition_x(contours[index_contour][:, 0].astype(int))

                    #a_ = np.max(y) - np.min(y)
                    #b_ = np.max(x) - np.min(x)
                    #label_area_ = a_ * b_

                    #if ((label_area_*100) / (label_area) >= 0.0): # relative ares should be greater than 1% compared to overall area of the entire mask
                    shape_attr = dict()
                    shape_attr['name'] = 'polygon'
                    shape_attr['all_points_x'] = x.tolist()
                    shape_attr['all_points_y'] = y.tolist()
                    shape_attribute = dict()
                    shape_attr['category_id'] = int(ii)
                    shape_attribute['shape_attributes'] = shape_attr
                    shape_attribute["region_attributes"] = {}
                    mask_like[x, y] = 255
                    regions[str(region_index)] = shape_attribute

                    if debug:
                         print("\nAdd region ",region_index," to dict.")
                    region_index += 1
                    #else:
                    #    print("relativ region size is smaller than 1%: ", (label_area_ * 100) / (label_area))

        return regions

    def create_segment(self,mask, segment_id):
            #py = np.where(mask > 0)[0].astype(np.uint8)
            #px = np.where(mask > 0)[1].astype(np.uint8)

            #poly = [(x, y) for x, y in zip(px, py)]
            #poly = [p for x in poly for p in x]

            idx = cv2.findContours(mask.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE, offset=(0,0))[1]
            out = np.zeros_like(mask)


            #px = np.where(out > 0)[1]#.astype(np.uint8)
            #py = np.where(out > 0)[0]#.astype(np.uint8)

            #poly = []
            #px = []
            #py = []
            for ii in range(len(idx)):
                conture = idx[ii]

                if np.shape(conture) > 10:
                    out[conture[:, 0, 1], conture[:, 0, 0]] = 255

                    px = np.where(out > 0)[1]  # .astype(np.uint8)
                    py = np.where(out > 0)[0]  # .astype(np.uint8)

                    poly = [(x, y) for x, y in zip(px, py)]
                    # poly = [p for x in poly for p in x]

                    segment = {
                        "bbox": [
                            np.min(px),
                            np.min(py),
                            np.max(px),
                            np.max(py)],
                        "bbox_mode": BoxMode.XYXY_ABS,
                        "segmentation": [poly],
                        "category_id": segment_id,
                    }

            return segment

    def create_sub_mask_annotation(self,sub_mask,c_id):#, image_id, category_id, annotation_id, is_crowd):
        # Find contours (boundary lines) around each sub-mask
        # Note: there could be multiple contours if the object
        # is partially occluded. (E.g. an elephant behind a tree)
        contours = measure.find_contours(sub_mask, 0.5, positive_orientation='low')


        segmentations = []
        polygons = []

        for contour in contours:

                # Flip from (row, col) representation to (x, y)
                # and subtract the padding pixel
                for i in range(len(contour)):
                    row, col = contour[i] # get coordinates (y, x)
                    contour[i] = (col - 1, row - 1)

                if len(contour) > 0:
                    # Make a polygon and simplify it
                    poly = Polygon(contour)
                    poly = poly.simplify(1.0, preserve_topology=False)

                    if poly.length > 0:
                        polygons.append(poly)
                        segmentation = np.array(poly.exterior.coords).ravel().tolist()
                        #if len(segmentation) > 5:
                        segmentations.append(segmentation)


        # Combine the polygons to calculate the bounding box and area
        multi_poly = MultiPolygon(polygons)
        x, y, max_x, max_y = multi_poly.bounds
        width = max_x - x
        height = max_y - y
        bbox = (x, y, width, height)
        area = multi_poly.area

        annotation = {
            'segmentation': segmentations,
        #    'iscrowd': is_crowd,
        #    'image_id': image_id,
            'category_id': c_id,
        #    'id': annotation_id,
            'bbox': bbox,
            "bbox_mode": BoxMode.XYWH_ABS,
            'area': area
        }

        return annotation

    def create_dataset_dict(self,imgs, labels, dataset_dict_file_path, merge_masks, dataset):


        debug = False
        new_method = True
        #dataset_dict = dict()#
        dataset_dict = []
        # iteration over each image
        for ii in tqdm(range(len(imgs))):

            try:

                img = cv2.imread(imgs[ii])
                print(imgs[ii])
                #if debug:
                #    imgs[ii] = "/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/20200416/images/frame_0223.png"
                #    img = cv2.imread("/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/20200416/images/frame_0223.png")
                    #/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/20200330/images/frame_0021.png 20200416 frame_0223
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

                label_mask = cv2.imread(labels[ii])

                #if debug:
                #    label_mask = cv2.imread("/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/20200416/labels/label_0223.png")
                label_mask = cv2.cvtColor(label_mask, cv2.COLOR_BGR2RGB)
            except BaseException:
                print("could not read image/label", imgs[ii])
                continue

            if (np.max(label_mask) == 0.0):
                print("\nLabel is black! \n", imgs[ii])
                continue

            #filename = os.path.basename(imgs[ii])
            height, width, channels = img.shape

            record = {}

            record["height"] = height
            record["width"] = width
            record["file_name"] = imgs[ii]
            record["image_id"] = ii

            segments = []
            #### get segments here

            #merged_masked = label_to_binary(mask=label_mask)

            #a = np.max(np.where(merged_masked > 0)[0]) - np.min(np.where(merged_masked > 0)[0])
            #b = np.max(np.where(merged_masked > 0)[1]) - np.min(np.where(merged_masked > 0)[1])

            #label_area = a * b

            if new_method:
                if merge_masks:
                    if np.max(merged_masked) > 0:
                        #segments.append(create_segment(mask=merged_masked, segment_id=1))

                        idx = cv2.findContours(merged_masked.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE, offset=(0, 0))[1]

                        for jj in range(len(idx)):
                            conture = idx[jj]
                            out = np.zeros_like(merged_masked)

                            if np.shape(conture)[0] > 30:
                                out[conture[:, 0, 1], conture[:, 0, 0]] = 255

                                px = np.where(out > 0)[1]  # .astype(np.uint8)
                                py = np.where(out > 0)[0]  # .astype(np.uint8)

                                poly = [(x, y) for x, y in zip(px, py)]
                                # poly = [p for x in poly for p in x]

                                segment = {
                                    "bbox": [
                                        np.min(px),
                                        np.min(py),
                                        np.max(px),
                                        np.max(py)],
                                    "bbox_mode": BoxMode.XYXY_ABS,
                                    "segmentation": [poly],
                                    "category_id": 1,
                                }
                                segments.append(segment)

                else:
                    objs = []

                    if dataset == "pancreas":
                        label_colours = get_pancreas_labels()

                    if dataset == "LM":
                        label_colours = get_LM_labels()

                    if dataset == "VD":
                        label_colours = get_VD_labels()

                    if dataset == "ME":
                        label_colours = get_ME_labels()

                    if dataset == "MM":
                        label_colours = get_MM_labels()

                    if dataset == "first":
                        label_colours = get_first_labels()

                    if dataset == "nerves":
                        label_colours = get_Nerves_labels()

                    if dataset == "PD":
                        label_colours = get_PD_labels()

                    for kk, label_rgb_val in enumerate(label_colours):
                        binary_mask = self.encode_segmap(mask=label_mask, label_value=label_rgb_val)

                        if np.max(binary_mask) > 0:  # sonst ist die maske leer

                            #objs += self.components(binary_mask, 1, kk, False)


                            #foo_mask = np.zeros((np.shape(binary_mask)[0] + 10, np.shape(binary_mask)[1] + 10),
                            #                    dtype=np.int64)

                            #foo_mask[5:np.shape(binary_mask)[0]:5,5:np.shape(binary_mask)[1]:5]=binary_mask

                            foo_mask = np.pad(binary_mask, (5, 5), mode='constant', constant_values=0.0)
                            try:
                                objs.append(self.create_sub_mask_annotation(sub_mask=foo_mask,c_id=kk))

                            except BaseException:
                                print("could not create annotation", imgs[ii]," for segment:", kk)
                                continue

                            continue
                            #segments.append(create_segment(mask=binary_mask,segment_id=ii))
                            idx = cv2.findContours(binary_mask.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE,
                                                  offset=(0, 0))[1]


                            for jj in range(len(idx)):
                                conture = idx[jj]
                                out = np.zeros_like(binary_mask)

                                if np.shape(conture)[0] > 0:
                                    out[conture[:, 0, 1], conture[:, 0, 0]] = 255

                                    px = np.where(out > 0)[1]  # .astype(np.uint8)
                                    py = np.where(out > 0)[0]  # .astype(np.uint8)

                                    poly = [(x, y) for x, y in zip(px, py)]
                                    #poly = [p for x in poly for p in x]

                                    segment = {
                                        "bbox": [
                                            np.min(px),
                                            np.min(py),
                                            np.max(px),
                                            np.max(py)],
                                        "bbox_mode": BoxMode.XYXY_ABS,
                                        "segmentation": [poly],
                                        "category_id": kk,
                                    }
                                    segments.append(segment)




            #if len(segments) > 0:
            record["annotations"] = objs#generate_regions(label_mask=label_mask,padding=5)#segments

            #dataset_dict[imgs[ii]] = record#
            dataset_dict.append(record)
            #else:
            #    print(imgs[ii], "\n No segments created!")

            del img, label_mask#, merged_masked
            #if debug:
            #    break

        with open(dataset_dict_file_path, 'wb') as f:
            #json.dump(dataset_dict, f)
            pickle.dump(dataset_dict, f, pickle.HIGHEST_PROTOCOL)#json.dump(dataset_dict, f)


        if debug:
            classes_list = ["pancreas_1", "pancreas_2", "pancreas_3", "stomach",""]


            for d in ["test"]:
                    DatasetCatalog.register(dataset + "_" + d, lambda d=d: self.load_dataset_dict(dataset_dict_file_path))
                    MetadataCatalog.get(dataset + "_" + d).set(thing_classes=classes_list)
                    MetadataCatalog.get(dataset + "_" + d).set(
                    #    thing_colors=[(249, 179, 110), (209, 151, 93), (147, 105, 66), (216, 131, 105)]
                        thing_colors=[(170, 84, 68), #abdominal wall
                                      (204, 168, 142),  #colon
                    (110, 184, 209),  #adhesion
                    (230, 219, 70), #fat
                    (255, 207, 177)#small intensine
                       ])

            pancreas_metadata = MetadataCatalog.get(dataset + "_" +"test")

            dataset_dicts = self.load_dataset_dict(dataset_dict_file_path)
            index_vis  = len([f for f in os.listdir("/dl_data/Experiments/LOOCV_001/checkup/") if os.path.isfile(os.path.join("/dl_data/Experiments/LOOCV_001/checkup/", f))]) + 1#0
            for d in dataset_dicts:
                    img = cv2.imread(d["file_name"])
                    #img = np.zeros(np.shape(img))#[:, :, ::-1]
                    #img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                    visualizer = Visualizer(img[:, :, ::-1], metadata=pancreas_metadata, scale=0.5,
                                            instance_mode=ColorMode.SEGMENTATION)
                    vis = visualizer.draw_dataset_dict(d)
                    cv2.imwrite("/dl_data/Experiments/LOOCV_001/checkup/frame_"+str(index_vis)+".png",
                                vis.get_image()[:, :, ::-1])
            #for d in random.sample(dataset_dicts, 200):

            #    img = cv2.imread(d["file_name"])

            #    visualizer = Visualizer(img[:, :, ::-1], metadata=dataset_metadata, scale=0.5,
#                                                instance_mode=ColorMode.SEGMENTATION)
            #    vis = visualizer.draw_dataset_dict(d)
            #    cv2.imwrite(os.path.join(self.checkup_path, "train_" + os.path.basename(d["file_name"])),
            #                        vis.get_image()[:, :, ::-1])
                    index_vis += 1

            DatasetCatalog.remove(dataset + "_" + "test")
        #    foo = 0

def parallel_process(data_obj):
    """
    Function for parallel feature extraction

    :param data_obj: input ExperimentClass object.
    :return:

    This function is used internally.
    """

    data_obj.create_dataset_description()

def process_images(data_obj_list,n_processes):
    """
    Process images for various tasks.

    :param data_config: full path to a data configuration xml file.
    :param settings_config: full path to a settings configuration xml file.
    :param n_processes: number of simultaneous processes. For n_processes > 1, parallel processes are started.
    :param keep_images_in_memory: flag to keep images in memory. This avoids repeated loading of images, but at the expense of memory.
    :param compute_features: flag to compute features (default: False)
    :param extract_images: flag to extract images and mask in Nifti format (default: False)
    :param plot_images: flag to plot images and masks as .png (default: False)
    :return:
    """

    import logging
    import multiprocessing as mp
    from inspect import stack, getmodule
    import pandas as pd
    import time

    # Extract features
    if n_processes > 1:

        # Usually, we would only run this code if __name__ == "__main__".
        # However, as this function is nested in a module, the __name__ is always "mirp.mainFunctions" instead of "__main__", thus prohibiting any multiprocessing.
        # Therefore we have to see if "__main__" is the name that appears in one of the parent modules. This is not always the top module, and therefore we search
        # the stack. The trick is that on windows OS "__main__" does appear in the calling environment from where the script is executed, but is called "__mp_main__"
        # in the multiprocessing spawning process. We perform this check to prevent infinite spawning on Windows OS, as it doesn't fork neatly but spawns identical
        # separate process by repeating the stack call in each process. The alternative would be to include a __name__ == "__main__" check in the calling script,
        # and a switch variable (e.g. as_master) in the function call, but that puts the onus on the end-user, and is a terrible idea due to the principle of least
        # astonishment.
        module_names = ["none"]
        for stack_entry in stack():
            current_module = getmodule(stack_entry[0])
            if current_module is not None:
                module_names += [current_module.__name__]

        if "__main__" in module_names:

            # Parse data




            # Initate process manager
            df_mngr = pd.DataFrame({"job_id": np.arange(len(data_obj_list)),
                                    "job_processed": np.zeros(shape=len(data_obj_list), dtype=np.bool),
                                    "job_in_process": np.zeros(shape=len(data_obj_list), dtype=np.bool),
                                    "assigned_worker": -np.ones(shape=len(data_obj_list), dtype=np.int),
                                    "error_iteration": np.zeros(shape=len(data_obj_list), dtype=np.int)})

            # Initiate worker list
            worker_list = []
            for ii in np.arange(n_processes):

                # Check if enough jobs are available
                if ii >= len(data_obj_list): break

                # Add job to worker
                process_name = "process " +str(ii)
                worker_list.append(mp.Process(target=parallel_process, args=(data_obj_list[ii],), name=process_name))
                worker_list[ii].daemon = True
                df_mngr.loc[ii, "assigned_worker"] = ii

            # Initiate a list that keeps track of repeated errors and skips those samples.
            error_skip_list = []

            # Iterate and process all jobs
            while np.any(~df_mngr.job_processed):

                # Start jobs
                for ii in np.arange(len(worker_list)):
                    # Check if worker is assigned
                    if ~np.any(df_mngr.assigned_worker == ii):
                        continue

                    # Get current job id
                    curr_job_id = df_mngr.job_id[df_mngr.assigned_worker == ii]

                    # Check if job is still in progress or was completed
                    if df_mngr.job_processed[curr_job_id].values or df_mngr.job_in_process[curr_job_id].values: continue

                    # Start process
                    df_mngr.loc[curr_job_id, "job_in_process"] = True
                    worker_list[ii].start()

                # No more workers are available
                free_workers = []

                # Check finished jobs - every 5 seconds
                while len(free_workers) == 0:
                    time.sleep(5)
                    for ii in np.arange(len(worker_list)):

                        # Check if worker is assigned
                        if ~np.any(df_mngr.assigned_worker == ii):
                            free_workers.append(ii)
                            continue

                        # Get current job id
                        curr_job_id = df_mngr.job_id[df_mngr.assigned_worker == ii].values[0]

                        # Check if worker is still processing
                        if worker_list[ii].is_alive(): continue

                        # Check exit code of the stopped worker
                        if worker_list[ii].exitcode == 0:
                            # Normal exit - update table and set worker
                            df_mngr.loc[curr_job_id, "job_processed"] = True
                            df_mngr.loc[curr_job_id, "job_in_process"] = False
                            df_mngr.loc[curr_job_id, "assigned_worker"] = -1

                            free_workers.append(ii)

                        else:
                            # This indicates some fault (e.g. segmentation fault)
                            df_mngr.loc[curr_job_id, "error_iteration"] += 1

                            # Stop after 2 iterations that produce errors
                            if df_mngr.loc[curr_job_id, "error_iteration"] < 2:
                                df_mngr.loc[curr_job_id, "job_in_process"] = False
                                df_mngr.loc[curr_job_id, "assigned_worker"] = -1

                                logging.warning("Process ended prematurely, attempting to restart.")
                            else:
                                df_mngr.loc[curr_job_id, "job_processed"] = True
                                df_mngr.loc[curr_job_id, "job_in_process"] = False
                                df_mngr.loc[curr_job_id, "assigned_worker"] = -1

                                error_skip_list.append(curr_job_id)

                                logging.warning("Process ended prematurely, no attempt to restart again.")

                            # Free up the worker
                            free_workers.append(ii)

                # Check remaining available jobs
                available_jobs = df_mngr.job_id[~np.logical_or(df_mngr.job_processed, df_mngr.job_in_process)]

                # Add new jobs to workers
                for jj in np.arange(len(free_workers)):

                    # Check if enough jobs are available
                    if jj >= len(available_jobs): break

                    # Add job to worker
                    sel_job_id = available_jobs.values[jj]
                    process_name = "New process " + str(jj)

                    worker_list[free_workers[jj]] = mp.Process(target=parallel_process,
                                                               args=(data_obj_list[sel_job_id],), name=process_name)
                    worker_list[free_workers[jj]].daemon = True
                    df_mngr.loc[sel_job_id, "assigned_worker"] = free_workers[jj]

            # Exit statement
            logging.info("Feature calculation has been completed.")

            #if len(error_skip_list) > 0:
            #    names = [data_obj_list[ii].modality + "_" + data_obj_list[ii].data_str + " of " + data_obj_list[ii].subject + " (" +
            #             data_obj_list[ii].cohort + ")" for ii in error_skip_list]
            #    logging.info("No features could be calculated for %s due to errors.", ", ".join(sample_name for sample_name in names))

    else:

        for data_obj in data_obj_list:
            data_obj.create_dataset_description()




if __name__ == '__main__':

    data_obj_list_train_valid = []
    data_obj_list_test = []
    export_img = False

    # Example for dataset with the name 'PD'
    dataset = "PD"

    # Specify the image path
    img_base_path = ""

    # Specify the names of the sub folders, in this example the images for each surgery is stored in separate folder
    sub_dirs = ["20200123", "20200129", "20200302", "20200416", "20200423", "20200622", "20200717", "20201002",
                "20201006"]

    # Define the name of the experiment folder
    exp_dir = "Experiments"

    # Define the kind of experiment, e.g. leave-one-out-cross validation (LOOCV) or n-fold cross validation (CV)
    exp_type = "LOOCV"

    # In the case of CV, define the number of folds and the number of repetitions
    n_splits = 3
    n_repeats = 3

    os.makedirs(os.path.join(img_base_path,exp_dir),exist_ok=True)

    index = 1

    if export_img:
        os.makedirs(os.path.join(out_base_path, exp_dir, "LOOCV_" + str(0).zfill(3)), exist_ok=True)
        base_folder = os.path.join(out_base_path, exp_dir, "LOOCV_" + str(0).zfill(3))

            # create subfolders
        os.makedirs(os.path.join(base_folder, "DATA"), exist_ok=True)
        os.makedirs(os.path.join(base_folder, "trained_models"), exist_ok=True)
        os.makedirs(os.path.join(base_folder, "performance"), exist_ok=True)
        os.makedirs(os.path.join(base_folder, "predictions"), exist_ok=True)
        os.makedirs(os.path.join(base_folder, "checkup"), exist_ok=True)


        data_obj_list_test.append(CreateDescrition(img_base_path=img_base_path,
                                                       sub_dirs=sub_dirs,
                                                       # padding=2,
                                                       purpose="test",
                                                       merge_masks=False,
                                                       dataset_dict_file_name=dataset,
                                                       dataset=dataset,
                                                       output_base_path=os.path.join(base_folder, "DATA")))


    if exp_type == "LOOCV":
        loocv = LeaveOneOut()
        loocv.get_n_splits(sub_dirs)
        for train_index, test_index in loocv.split(sub_dirs):

            train_val_sub_dirs = [sub_dirs[i] for i in train_index]
            test_sub_dirs = [sub_dirs[i] for i in test_index]
            print("TRAIN:", train_val_sub_dirs, "TEST:", test_sub_dirs)

            #create base folder
            os.makedirs(os.path.join(img_base_path,exp_dir,"LOOCV_"+str(index).zfill(3)),exist_ok=True)
            base_folder = os.path.join(img_base_path,exp_dir,"LOOCV_"+str(index).zfill(3))

            #create subfolders
            os.makedirs(os.path.join(base_folder,"DATA"),exist_ok=True)
            os.makedirs(os.path.join(base_folder, "trained_models"),exist_ok=True)
            os.makedirs(os.path.join(base_folder, "performance"),exist_ok=True)
            os.makedirs(os.path.join(base_folder, "predictions"),exist_ok=True)
            os.makedirs(os.path.join(base_folder, "checkup"), exist_ok=True)



            data_obj_list_train_valid.append(CreateDescrition(img_base_path=img_base_path,
                                       sub_dirs=train_val_sub_dirs,
                                       #padding=2,
                                       purpose="train_val",
                                       merge_masks=False,
                                       dataset_dict_file_name=dataset,
                                                              dataset=dataset,
                                       output_base_path=os.path.join(base_folder,"DATA")))

            data_obj_list_test.append(CreateDescrition(img_base_path=img_base_path,
                                                  sub_dirs=test_sub_dirs,
                                                  # padding=2,
                                                  purpose="test",
                                                  merge_masks=False,
                                                  dataset_dict_file_name=dataset,
                                                       dataset=dataset,
                                                  output_base_path=os.path.join(base_folder, "DATA")))

            index = index + 1

    if exp_type == "CV":

        rkf = RepeatedKFold(n_splits=n_splits, n_repeats=n_repeats, random_state=2652124)
        for train_index, test_index in rkf.split(sub_dirs):

            train_val_sub_dirs = [sub_dirs[i] for i in train_index]
            test_sub_dirs = [sub_dirs[i] for i in test_index]
            print("TRAIN:", train_val_sub_dirs, "TEST:", test_sub_dirs)

            # create base folder
            os.makedirs(os.path.join(img_base_path, exp_dir, "CV_" + str(index).zfill(3)), exist_ok=True)
            base_folder = os.path.join(img_base_path, exp_dir, "CV_" + str(index).zfill(3))

            # create subfolders
            os.makedirs(os.path.join(base_folder, "DATA"), exist_ok=True)
            os.makedirs(os.path.join(base_folder, "trained_models"), exist_ok=True)
            os.makedirs(os.path.join(base_folder, "performance"), exist_ok=True)
            os.makedirs(os.path.join(base_folder, "predictions"), exist_ok=True)
            os.makedirs(os.path.join(base_folder, "checkup"), exist_ok=True)


            data_obj_list_train_valid.append(CreateDescrition(img_base_path=img_base_path,
                                                              sub_dirs=train_val_sub_dirs,
                                                              # padding=2,
                                                              purpose="train_val",
                                                              merge_masks=False,
                                                              dataset_dict_file_name=dataset,
                                                              dataset=dataset,
                                                              #img_example_base_path = os.path.join(base_folder, "checkup"),
                                                              output_base_path=os.path.join(base_folder, "DATA")))

            data_obj_list_test.append(CreateDescrition(img_base_path=img_base_path,
                                                       sub_dirs=test_sub_dirs,
                                                       # padding=2,
                                                       purpose="test",
                                                       merge_masks=False,
                                                       dataset_dict_file_name=dataset,
                                                       dataset=dataset,
                                                       # img_example_base_path=os.path.join(base_folder, "checkup"),
                                                       output_base_path=os.path.join(base_folder, "DATA")))

            index = index + 1


    process_images(data_obj_list=data_obj_list_train_valid,n_processes=10)

    process_images(data_obj_list=data_obj_list_test,n_processes=10)



