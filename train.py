
from create_dataset import get_PD_labels,get_PD_label_desc,get_pancreas_labels, get_pancreas_label_desc, get_pancreas_labels_for_vis ,get_Nerves_label_desc,get_Nerves_labels,get_LM_labels, get_ME_labels,get_VD_labels,get_MM_labels,get_first_labels, get_VD_label_desc, get_LM_label_desc,get_VD_label_desc,get_ME_label_desc,get_MM_label_desc,get_first_label_desc
import os
from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg
from detectron2 import model_zoo
import torch
import json
import numpy as np
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.structures import BoxMode
import random
import cv2
from detectron2.utils.visualizer import Visualizer
import sklearn.metrics as metrics

import json
from tqdm import tqdm
from detectron2.engine import DefaultPredictor
from detectron2.utils.visualizer import ColorMode, VisImage
from argparse import ArgumentParser
import logging
import pandas as pd
import glob

import matplotlib
import matplotlib as mpl
import matplotlib.colors as mplc
import matplotlib.figure as mplfigure
import pycocotools.mask as mask_util

import matplotlib.pyplot as plt
import pickle

font = {'family' : 'Arial',
        'size' : 10}

matplotlib.rc('font',**font)

NUM_WORKERS = 8
BATCHSIZE = 2
LR = 0.0004#0.00025

MAX_EPOCHS = 200
#MAXITER = np.int(MAX_EPOCHS * 1550 / (BATCHSIZE))#80000#400000
ROI_HEADS_BATCH_SIZE_PER_IMAGE = 128


def get_binary_masks(mask, dataset):
    """Encode segmentation label images as pascal classes
    Args:
        mask (np.ndarray): raw segmentation label image of dimension
          (M, N, 3), in which the Pascal classes are encoded as colours.
    Returns:
        (np.ndarray): class map with dimensions (M,N), where the value at
        a given location is the integer denoting the class index.
    """
    mask = mask.astype(int)
    label_masks = {}

    if dataset == "pancreas":
        label_colours = get_pancreas_labels()

    if dataset == "LM":
        label_colours = get_LM_labels()

    if dataset == "VD":
        label_colours = get_VD_labels()

    if dataset == "ME":
        label_colours = get_ME_labels()

    if dataset == "MM":
        label_colours = get_MM_labels()

    if dataset == "first":
        label_colours = get_first_labels()

    if dataset == "nerves":
        label_colours = get_Nerves_labels()

    if dataset == "PD":
        label_colours = get_PD_labels()

    #if dataset == "liver":
    #    label_colours = get


    for ii, label in enumerate(label_colours):
        label_mask = np.zeros((mask.shape[0], mask.shape[1]), dtype=np.int16)
        label_mask[np.where(np.all(mask == label, axis=-1))[:2]] = 1
        label_mask = label_mask.astype(int)

        if np.max(label_mask) > 0:
            label_masks[ii]=label_mask

    return label_masks

def jaccard_ternaus(y_true, y_pred):
    """
    is the implementation of the jaccard index by Ternaus

    Returns:
        a floating point value as a score
    """
    intersection = (y_true * y_pred).sum()
    union = y_true.sum() + y_pred.sum() - intersection
    return (intersection + 1e-15) / (union + 1e-15)

def load_dataset_dict(dataset_dict_file):

    with open(dataset_dict_file, 'rb') as f:
        dataset_dict = pickle.load(f)

        #MAXITER = np.int(MAX_EPOCHS * 1550 / (MAXITER * BATCHSIZE))
        #print("max iteration set to: ",MAXITER)
        return dataset_dict

    return None

def train_net(exp_path, dataset="pancreas", resume=False, model_path=None):

    if dataset == "pancreas_extended":
        classes_list = ["pancreas"]

    if dataset == "liver":
        classes_list = ["liver"]

    if dataset == "veins":
        classes_list = ["veins"]

    if dataset == "artery":
        classes_list = ["artery"]


    if dataset == "spleen":
        classes_list = ["spleen"]

    if dataset == "ureter":
        classes_list = ["ureter"]

    if dataset == "stomach":
        classes_list = ["stomach"]

    if dataset == "pancreas":
        classes_list = ["pancreas_1","pancreas_2","pancreas_3","stomach"]
        #classes_list = ["pancreas", "pancreas"]

    if dataset == "VD":
        classes_list = ["inferior mesenteric artery", "inferior mesenteric vein", "plastic clip", "titan clip"]

    if dataset == "LM":
        classes_list = ["abdominal_wall","colon","adhesion","fat","small_intestine"]

    if dataset == "MM":
        classes_list = ["gerotas fascia","mesocolon","dissection line gerota","exploration area"]

    if dataset == "ME":
        classes_list = ["dissection plane TME", "dissection line TME", "rectum", "seminal vesicles"]

    if dataset == "first":
        classes_list = [ "plane first incision",
    "first incision",
 "incised area",
 "instrument fenestrated bipolar forceps",
 "instrument permanent cautery hook",
"instrument monopolar curved scissors",
 "instrument cadiere forceps",
 "compress",
    "background"]

    if dataset == "nerves":
        classes_list = ["nerve","artery","vein"]


    if dataset == "PD":
        classes_list = ["pedicle package",
                       "inferior mesenteric artery"]

    print("Start training for Experiment ",os.path.basename(exp_path)," and classes: ",classes_list)

    for d in ["train"]:
        DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(os.path.join(exp_path,"DATA",dataset + "_" + d+".pkl")))
        MetadataCatalog.get(dataset + "_" + d).set(thing_classes=classes_list)

        if dataset == "pancreas_extended" or dataset == "liver" or dataset == "spleen" or dataset == "ureter" or dataset == "stomach" or dataset == "veins" or dataset == "artery":
            MetadataCatalog.get(dataset + "_" + d).set(thing_colors=[(249, 179, 110)])

        if dataset == "pancreas":
            MetadataCatalog.get( dataset + "_" + d).set(
                thing_colors=[(249, 179, 110), (209, 151, 93), (147, 105, 66), (216, 131, 105)])

        if dataset == "LM":
            MetadataCatalog.get(dataset + "_" + d).set(
                thing_colors=[(170, 84, 68), #abdominal wall
                       (204, 168, 142),  #colon
                       (110, 184, 209),  #adhesion
            (230, 219, 70), #fat
                       (255, 207, 177)#small intensine
                       ])

        if dataset == "first":
            MetadataCatalog.get(dataset + "_" + d).set(
                thing_colors=[(128,77,0),  # first incision
                              (230,0,205),  # plane first incesion
                              (255, 255, 0),  # incised area
                              (0, 0, 126),  # instrument
                              (96, 66, 54),  # instrument
                              (0,174,4),  # instrument
                              (84,84,126),
                              (255, 255, 255),  # compress
                              (170, 255, 255)
                              ])

        if dataset == "MM":
            MetadataCatalog.get(dataset + "_" + d).set(
                thing_colors=[(151,188,207),
                       (128,174,128),
                       (219,244,20),
                       (255,61,193)
                       ])

        if dataset == "VD":
            MetadataCatalog.get(dataset + "_" + d).set(
                thing_colors=[(216,100,79),
                       (0,151,205),
                       (184,232,61),
                       (230,168,28)
                       ])

        if dataset == "ME":
            MetadataCatalog.get(dataset + "_" + d).set(
                thing_colors=[(38,121,26),
                       (255,0,0),
                       (160,130,112),
                       (244,172,147)
                       ])

        if dataset == "nerves":
            MetadataCatalog.get(dataset + "_" + d).set(
                thing_colors=[(244, 214, 49),  #nerve
                              (216, 100, 79),  # artery
                              (0, 151, 205)
                       ])

        if dataset == "PD":
            MetadataCatalog.get(dataset + "_" + d).set(
                thing_colors=[(244, 214, 49),  #nerve
                       (216, 100, 79)
                       ])

        dataset_metadata = MetadataCatalog.get(dataset + "_train")

        dataset_dicts = load_dataset_dict(os.path.join(exp_path,"DATA",dataset + "_" + d +".pkl"))

    logging.basicConfig(filename=os.path.join(exp_path,"checkup","filepaths.log"), level=logging.INFO)
    for d in random.sample(dataset_dicts, 200):
        logging.info(d["file_name"])
        img = cv2.imread(d["file_name"])

        visualizer = Visualizer(img[:, :, ::-1], metadata=dataset_metadata, scale=0.5, instance_mode=ColorMode.SEGMENTATION)
        vis = visualizer.draw_dataset_dict(d)
        cv2.imwrite(os.path.join(exp_path,"checkup",os.path.basename(d["file_name"])),
                    vis.get_image()[:, :, ::-1])


    cfg = get_cfg()
    cfg.MODEL.DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
    #cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
    cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"))

    cfg.DATASETS.TRAIN = (dataset + "_train",)
    cfg.DATASETS.TEST = ()
    cfg.DATALOADER.NUM_WORKERS = NUM_WORKERS
    #cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml")

    if resume:
        cfg.MODEL.WEIGHTS = model_path

    cfg.SOLVER.IMS_PER_BATCH = BATCHSIZE


    cfg.SOLVER.BASE_LR = LR

    if resume:
        cfg.SOLVER.MAX_ITER = np.int(MAX_EPOCHS * len(dataset_dicts) / (BATCHSIZE)) #+ 20000

        #cfg.SOLVER.BASE_LR = 0.005
    else:
        cfg.SOLVER.MAX_ITER = np.int(MAX_EPOCHS * len(dataset_dicts) / (BATCHSIZE))

    print("Set MAX_ITER to: ",cfg.SOLVER.MAX_ITER)

    cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = ROI_HEADS_BATCH_SIZE_PER_IMAGE

    if dataset == "pancreas_extended" or dataset == "liver" or dataset == "spleen" or dataset == "ureter" or dataset == "stomach" or dataset == "veins" or dataset == "artery":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1

    if dataset == "pancreas":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_pancreas_labels())

    if dataset == "LM":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_LM_labels())

    if dataset == "VD":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_VD_labels())

    if dataset == "ME":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_ME_labels())

    if dataset == "MM":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_MM_labels())

    if dataset == "first":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_first_labels())

    if dataset == "nerves":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_Nerves_labels())

    if dataset == "PD":
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_PD_labels())

    # where the model is stored or where we want to save it
    cfg.OUTPUT_DIR = os.path.join(exp_path,"trained_models")

    trainer = DefaultTrainer(cfg)
    trainer.resume_or_load(resume=resume)
    trainer.train()


def get_confusion_matrix_elements(groundtruth_list, predicted_list):

    import sklearn.metrics

    import scipy.misc
    import scipy.ndimage
    import skimage.filters

    #_assert_valid_lists(groundtruth_list, predicted_list)

    #if _all_class_1_predicted_as_class_1(groundtruth_list, predicted_list) is True:
    #    tn, fp, fn, tp = 0, 0, 0, np.float64(len(groundtruth_list))

    #elif _all_class_0_predicted_as_class_0(groundtruth_list, predicted_list) is True:
    #    tn, fp, fn, tp = np.float64(len(groundtruth_list)), 0, 0, 0

    #else:

    return specificity(seg=predicted_list,ground=groundtruth_list)
    #tn, fp, fn, tp = sklearn.metrics.confusion_matrix(groundtruth_list, predicted_list).ravel()
    #tn, fp, fn, tp = np.float64(tn), np.float64(fp), np.float64(fn), np.float64(tp)

    #return tn, fp, fn, tp

def specificity(seg,ground):
    #computes false positive rate
    num=np.sum(np.multiply(ground==0, seg ==0))
    denom=np.sum(ground==0)
    if denom==0:
        return 1
    else:
        return  num/denom

def inference(exp_path, purpose, dataset, specific_model, vis_img=False):

    print("start inference")
    models = glob.glob(os.path.join(exp_path, "trained_models", specific_model + ".pth"))
    global_average_jaccard = 0.0

    df_global = None
    if dataset == "pancreas":
        classes_list = ["pancreas_1", "pancreas_2", "pancreas_3", "stomach"]

    if dataset == "pancreas_binary":
        classes_list = ["pancreas", "pancreas"]

    if dataset == "VD":
        classes_list = ["inferior mesenteric artery", "inferior mesenteric vein", "plastic clip", "titan clip"]

    if dataset == "LM":
        classes_list = ["abdominal_wall","colon","adhesion","fat","small_intestine"]

    if dataset == "MM":
        classes_list = ["gerotas fascia","mesocolon","dissection line gerota","exploration area"]

    if dataset == "ME":
        classes_list = ["dissection plane TME", "dissection line TME", "rectum", "seminal vesicles"]

    if dataset == "first":
        classes_list = [ "plane first incision",
    "first incision",
 "incised area",
 "instrument fenestrated bipolar forceps",
 "instrument permanent cautery hook",
"instrument monopolar curved scissors",
 "instrument cadiere forceps",
 "compress",
    "background"]

    if dataset == "nerves":
        classes_list = ["nerve","artery","vein"]

    if dataset == "PD":
        classes_list = ["pedicle package",
                       "inferior mesenteric artery"]

    if purpose == "train":
        for d in ["train"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))


    if purpose == "valid":
        for d in ["val"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))


    if purpose == "test":
        for d in ["test"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))

    if dataset == "pancreas":
        MetadataCatalog.get(dataset + "_" + d).set(
                    thing_colors=[(249, 179, 110), (209, 151, 93), (147, 105, 66), (216, 131, 105)])

    if dataset == "pancreas_binary":
        MetadataCatalog.get(dataset + "_" + d).set(
                    thing_colors=[(249, 179, 110), (249, 179, 110)])

    if dataset == "LM":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(170, 84, 68),  # abdominal wall
                          (204, 168, 142),  # colon
                          (110, 184, 209),  # adhesion
                          (230, 219, 70),  # fat
                          (255, 207, 177)  # small intensine
                          ])

    if dataset == "first":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(128,77,0),  # first incision
                              (230,0,205),  # plane first incesion
                              (255, 255, 0),  # incised area
                              (0, 0, 126),  # instrument
                              (96, 66, 54),  # instrument
                              (0,174,4),  # instrument
                              (84,84,126),
                              (255, 255, 255),  # compress
                              (170, 255, 255)
                          ])

    if dataset == "MM":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(151, 188, 207),
                          (128, 174, 128),
                          (219, 244, 20),
                          (255, 61, 193)
                          ])

    if dataset == "VD":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(216, 100, 79),
                          (0, 151, 205),
                          (184, 232, 61),
                          (230, 168, 28)
                          ])

    if dataset == "ME":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(38, 121, 26),
                          (255, 0, 0),
                          (160, 130, 112),
                          (244, 172, 147)
                          ])

    if dataset == "nerves":
        MetadataCatalog.get(dataset + "_" + d).set(
                thing_colors=[(244, 214, 49),  #nerve
                              (216, 100, 79),  # artery
                              (0, 151, 205)
                       ])

    if dataset == "PD":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(244, 214, 49),  # nerve
                          (216, 100, 79)
                          ])

    if dataset == "pancreas_extended" or dataset == "liver" or dataset == "spleen" or dataset == "ureter" or dataset == "stomach" or dataset == "veins" or dataset == "artery":
        MetadataCatalog.get(dataset + "_" + d).set(thing_colors=[(249, 179, 110)])

    for model_index in range(len(models)):
        local_average_jaccard = 0.0

        model_path = models[model_index]

        print("Check model: ", os.path.basename(model_path))
        cfg = get_cfg()
        cfg.MODEL.DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
        print(cfg.MODEL.DEVICE)
        cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"))

        if dataset == "pancreas_extended":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
            label_desc = np.asarray(["pancreas"])

        if dataset == "liver":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
            label_desc = np.asarray(["liver"])

        if dataset == "spleen":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
            label_desc = np.asarray(["spleen"])

        if dataset == "ureter":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
            label_desc = np.asarray(["ureter"])

        if dataset == "stomach":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
            label_desc = np.asarray(["stomach"])

        if dataset == "veins":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
            label_desc = np.asarray(["veins"])

        if dataset == "artery":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = 1
            label_desc = np.asarray(["artery"])

        if dataset == "pancreas":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_pancreas_labels())
            label_desc = get_pancreas_label_desc()

        if dataset == "LM":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_LM_labels())
            label_desc = get_LM_label_desc()

        if dataset == "VD":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_VD_labels())
            label_desc = get_VD_label_desc()

        if dataset == "ME":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_ME_labels())
            label_desc = get_ME_label_desc()

        if dataset == "MM":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_MM_labels())
            label_desc = get_MM_label_desc()

        if dataset == "first":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_first_labels())
            label_desc = get_first_label_desc()

        if dataset == "nerves":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_Nerves_labels())
            label_desc = get_Nerves_label_desc()

        if dataset == "PD":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_PD_labels())
            label_desc = get_PD_label_desc()

        cfg.MODEL.WEIGHTS = model_path

        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8

        predictor = DefaultPredictor(cfg)

        segment_jaccard = {}

        if purpose == "train":
            dataset_metadata = MetadataCatalog.get(dataset + "_train")

            data_dict = load_dataset_dict(os.path.join(exp_path, "DATA", dataset + "_train.pkl"))

        if purpose == "valid":
            dataset_metadata = MetadataCatalog.get(dataset + "_val")

            data_dict = load_dataset_dict(os.path.join(exp_path, "DATA", dataset + "_val.pkl"))

        if purpose == "test":
            dataset_metadata = MetadataCatalog.get(dataset + "_test")

            data_dict = load_dataset_dict(os.path.join(exp_path, "DATA", dataset + "_test.pkl"))

        os.makedirs(os.path.join(exp_path, "predictions", purpose), exist_ok=True)

        df = pd.DataFrame(columns=['model', 'label_id', 'label_name', 'metric', 'value', 'comment', 'file_path'])

        label_number = 0
        sample_index = 0
        segment_jaccard = {}
        for d in tqdm(data_dict):

            image_jaccard_index = {}

            if dataset == "pancreas_extended" or dataset == "liver" or dataset == "spleen" or dataset == "ureter" or dataset == "stomach" or dataset == "veins" or dataset == "artery":
                label_name = d["file_name"].replace("image", "mask")
            else:
                label_name = d["file_name"].replace("frame", "label").replace("images", "labels")

            try:
                    im = cv2.imread(d["file_name"])

                    #im = cv2.imread("/dl_data/Pancreas/20200330/images/frame_0061.png")
                    #im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

                    op_name = d["file_name"].split("/")[len(d["file_name"].split("/"))-3]

                    if dataset == "pancreas_extended" or dataset == "liver" or dataset == "spleen" or dataset == "ureter" or dataset == "stomach" or dataset == "veins" or dataset == "artery":
                        label = cv2.imread(label_name,0)
                    else:
                        label = cv2.imread(label_name)
                        #label = cv2.imread("/dl_data/Pancreas/20200330/labels/label_0061.png")
                        label = cv2.cvtColor(label, cv2.COLOR_BGR2RGB)

            except BaseException:
                print("could not read image/label", d["file_name"])
                continue

            outputs = predictor(im)

            predicted_label = list(outputs['instances']._fields['pred_classes'].data.cpu().numpy())
            predicted_masks = list(outputs['instances']._fields['pred_masks'].data.cpu().numpy())
            #predicted_boxes = list(outputs['instances']._fields['pred_boxes'].tensor.cpu().data.numpy())

            # führe predicted labels zusammen,
            # falls diese zum gleichen label gehören

            predictions = {}
            for ii in range(len(predicted_masks)):

                if predicted_label[ii] in predictions:
                    predictions[predicted_label[ii]] = np.logical_or(predicted_masks[ii], predictions[predicted_label[ii]])
                else:
                    predictions[predicted_label[ii]] = predicted_masks[ii]

            if dataset == "pancreas_extended" or dataset == "liver" or dataset == "spleen" or dataset == "ureter" or dataset == "stomach":
                label_masks = {}
                label_masks[0] = label
            else:
                label_masks = get_binary_masks(mask=label, dataset=dataset)

            for label_keys, label in label_masks.items():

                # füge label zu der jaccard liste hinzu,
                # falls dieses Label nicht bereits vorhanden ist
                if not label_keys in segment_jaccard:
                    segment_jaccard[label_keys] = []

                # prüfe ob alle annotierten labels vorhergesagt wurden,
                # falls nicht füge einen null eintrag zur liste
                if not label_keys in predicted_label:
                    # print("Give label: ",key," was not found in predicted labels!")
                    jaccard_list = segment_jaccard[label_keys]
                    jaccard_list.append(0.0)
                    segment_jaccard[label_keys] = jaccard_list

                    image_jaccard_index[label_keys] = 0.0
                    ternaus_jaccard = 0.0

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["jaccard_score_ternaus"] + [
                        str(ternaus_jaccard)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["jaccard_score"] + [
                        str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["f1_score"] + [
                        str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["precision_score"] + [
                        str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["recall_score"] + [
                        str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["specificity_score"] + [
                        str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                else:
                    jaccard_list = segment_jaccard[label_keys]

                    y_true = label_masks[label_keys]
                    y_pred = predictions[label_keys]
                    ternaus_jaccard = jaccard_ternaus(
                        y_true=y_true.astype(bool).flatten(),
                        y_pred=y_pred.astype(bool).flatten())

                    jaccard_list.append(ternaus_jaccard)

                    image_jaccard_index[label_keys] = ternaus_jaccard

                    jaccard_score = metrics.jaccard_score(y_true=y_true.astype(bool).flatten(),
                        y_pred=y_pred.astype(bool).flatten())

                    f1_score = metrics.f1_score(y_true=y_true.astype(bool).flatten(),
                        y_pred=y_pred.astype(bool).flatten())

                    pre_score = metrics.precision_score(y_true=y_true.astype(bool).flatten(),
                        y_pred=y_pred.astype(bool).flatten(), zero_division=0)

                    recall_score = metrics.recall_score(y_true=y_true.astype(bool).flatten(),
                        y_pred=y_pred.astype(bool).flatten(), zero_division=0)

                    #tn, fp, fn, tp = get_confusion_matrix_elements(groundtruth_list=y_true.flatten(), predicted_list=y_pred.flatten())
                    #specificity_score = tn / (tn+fp)

                    specificity_score = get_confusion_matrix_elements(groundtruth_list=y_true.flatten(), predicted_list=y_pred.flatten())

                    segment_jaccard[label_keys] = jaccard_list

                    # add singe segment to df for statistc summary

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["jaccard_score_ternaus"] + [
                        str(ternaus_jaccard)] + [""] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["jaccard_score"] + [
                        str(jaccard_score)] + [""] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["f1_score"] + [
                        str(f1_score)] + [""] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["precision_score"] + [
                        str(pre_score)] + [""] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["recall_score"] + [
                        str(recall_score)] + [""] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(label_keys)] + [label_desc[label_keys]] + ["specificity_score"] + [
                        str(specificity_score)] + [""] + [d["file_name"]]
                    label_number = label_number + 1


            #if sample_index == 5:
            #    break
            #sample_index = sample_index + 1

            if vis_img:
                v = Visualizer(im[:, :, ::-1], metadata=dataset_metadata, scale=0.75, instance_mode=ColorMode.SEGMENTATION)

                out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
                # cv2.imwrite(
                #        os.path.join(exp_path,"predictions",purpose,op_name+"_"+os.path.basename(d["file_name"])),
                #        out.get_image()[:, :, ::-1])

                visualizer = Visualizer(im[:, :, ::-1], metadata=dataset_metadata, scale=0.75,
                                        instance_mode=ColorMode.SEGMENTATION)
                vis_org = visualizer.draw_dataset_dict(d)
                # cv2.imwrite(os.path.join(exp_path, "checkup", os.path.basename(d["file_name"])),
                #            vis_org.get_image()[:, :, ::-1])

                fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(18, 10))

                ax1.set_title('Annotated Image')
                ax1.imshow(vis_org.get_image())
                ax1.axes.get_yaxis().set_visible(False)
                ax1.axes.get_xaxis().set_visible(False)

                ax2.set_title('Predicted Image')
                ax2.imshow(out.get_image())
                ax2.axes.get_yaxis().set_visible(False)
                ax2.axes.get_xaxis().set_visible(False)

                str_single_jaccard = ""
                # for key, val in image_jaccard_index.items():
                #    str_single_jaccard += label_desc[key] +": J=" + str(image_jaccard_index[key]) + "; "

                from statistics import mean
                aj = mean(image_jaccard_index.values())
                str_single_jaccard += "AJ=" + str(aj)
                fig.suptitle("OP: " + op_name + "\n Image: " + os.path.basename(
                    d["file_name"]) + "\n " + str_single_jaccard)  # + "\n AJ:" + str(np.round(aj,3)))
                # plt.axis('off')
                plt.savefig(
                    os.path.join(exp_path, "predictions", purpose, op_name + "_" + os.path.basename(d["file_name"])),
                    dpi=300)
                plt.close('all')

        for key, val in segment_jaccard.items():
            local_average_jaccard += np.mean(val)

        print(local_average_jaccard / np.float(len(segment_jaccard)))
        if (local_average_jaccard / np.float(len(segment_jaccard))) > global_average_jaccard:
            print("Found new best model", os.path.basename(model_path), "with AJ:",
                      local_average_jaccard / np.float(len(segment_jaccard)))
            global_average_jaccard = local_average_jaccard / np.float(len(segment_jaccard))
            #final_model_path = model_path
            df_global = df.copy()
            #print(df_global)
            #print("hallo")


    #if not df_global.empty():
    #print(df_global)
    df_global.to_csv(os.path.join(exp_path,"performance","performance_"+purpose+".csv"))

    if purpose == "valid":
        DatasetCatalog.remove(dataset + "_val")
    else:
        DatasetCatalog.remove(dataset + "_" + purpose)

def infer_incision_plane(exp_path, purpose, dataset, specific_model, vis_img=False):

    print("start infer_incision_plane")
    models = glob.glob(os.path.join(exp_path, "trained_models", specific_model + ".pth"))
    global_average_jaccard = 0.0


    if dataset == "first":
        classes_list = [ "plane first incision",
    "first incision",
 "incised area",
 "instrument fenestrated bipolar forceps",
 "instrument permanent cautery hook",
"instrument monopolar curved scissors",
 "instrument cadiere forceps",
 "compress",
    "background"]
    else:
        return


    if purpose == "train":
        for d in ["train"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))


    if purpose == "valid":
        for d in ["val"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))


    if purpose == "test":
        for d in ["test"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))


    if dataset == "first":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(128,77,0),  # first incision
                              (230,0,205),  # plane first incesion
                              (255, 255, 0),  # incised area
                              (0, 0, 126),  # instrument
                              (96, 66, 54),  # instrument
                              (0,174,4),  # instrument
                              (84,84,126),
                              (255, 255, 255),  # compress
                              (170, 255, 255)
                          ])

    for model_index in range(len(models)):

        model_path = models[model_index]

        print("Check model: ", os.path.basename(model_path))
        cfg = get_cfg()
        cfg.MODEL.DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
        print(cfg.MODEL.DEVICE)
        cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"))


        if dataset == "first":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_first_labels())
            label_desc = get_first_label_desc()


        cfg.MODEL.WEIGHTS = model_path

        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8

        predictor = DefaultPredictor(cfg)

        segment_jaccard = {}

        if purpose == "train":
            dataset_metadata = MetadataCatalog.get(dataset + "_train")

            data_dict = load_dataset_dict(os.path.join(exp_path, "DATA", dataset + "_train.pkl"))

        if purpose == "valid":
            dataset_metadata = MetadataCatalog.get(dataset + "_val")

            data_dict = load_dataset_dict(os.path.join(exp_path, "DATA", dataset + "_val.pkl"))

        if purpose == "test":
            dataset_metadata = MetadataCatalog.get(dataset + "_test")

            data_dict = load_dataset_dict(os.path.join(exp_path, "DATA", dataset + "_test.pkl"))

        os.makedirs(os.path.join(exp_path, "prediction_incision_plane", purpose), exist_ok=True)
        os.makedirs(os.path.join(exp_path, "prediction_incision_plane", purpose,"raw"), exist_ok=True)
        os.makedirs(os.path.join(exp_path, "prediction_incision_plane", purpose,"pred"), exist_ok=True)


        for d in tqdm(data_dict):


            label_name = d["file_name"].replace("frame", "label").replace("images", "labels")

            try:
                    im = cv2.imread(d["file_name"].replace("/media/data/datasets/CoBot","/dl_data"))

                    #im = cv2.imread("/dl_data/Pancreas/20200330/images/frame_0061.png")
                    #im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

                    op_name = d["file_name"].split("/")[len(d["file_name"].split("/"))-3]

                    label = cv2.imread(label_name.replace("/media/data/datasets/CoBot","/dl_data"))
                        #label = cv2.imread("/dl_data/Pancreas/20200330/labels/label_0061.png")
                    label = cv2.cvtColor(label, cv2.COLOR_BGR2RGB)

            except BaseException:
                print("could not read image/label", d["file_name"])
                continue


            label_masks = get_binary_masks(mask=label, dataset=dataset)
            # Nur wenn die plane und die line enthalten ist machen wir weiter


            if 0 in label_masks.keys() and 1 in label_masks.keys():
                outputs = predictor(im)

                predicted_label = list(outputs['instances']._fields['pred_classes'].data.cpu().numpy())
                predicted_masks = list(outputs['instances']._fields['pred_masks'].data.cpu().numpy())

                # führe predicted labels zusammen,
                # falls diese zum gleichen label gehören

                predictions = {}
                for ii in range(len(predicted_masks)):

                    if predicted_label[ii] in predictions:
                        predictions[predicted_label[ii]] = np.logical_or(predicted_masks[ii], predictions[predicted_label[ii]])
                    else:
                        predictions[predicted_label[ii]] = predicted_masks[ii]

                img_true = {}
                img_pred = {}

                # plane
                img_true[0] = label_masks[0]

                if 0 in predictions:
                    img_pred[0] = predictions[0]

                # line


                img_true[1] = label_masks[1]

                if 1 in predictions:
                    img_pred[0] = predictions[1]

                create_rgb_img(label_mask=img_true,
                               image_shape=np.shape(label),
                               segment_colours=[[128, 77, 0],[230, 0, 205]],
                               segment_label_value=1,
                               output_folder=os.path.join(exp_path, "prediction_incision_plane", purpose,"raw",op_name+"_"+os.path.basename(d["file_name"])))

                create_rgb_img(label_mask=img_pred,
                               image_shape=np.shape(label),
                               segment_colours=[[128, 77, 0],[230, 0, 205]],
                               segment_label_value=1,
                               output_folder=os.path.join(exp_path, "prediction_incision_plane", purpose,"pred",op_name+"_"+os.path.basename(d["file_name"])))



            #if sample_index == 5:
            #    break
            #sample_index = sample_index + 1


    if purpose == "valid":
        DatasetCatalog.remove(dataset + "_val")
    else:
        DatasetCatalog.remove(dataset + "_" + purpose)

def infer_incision_plane_observer(exp_path, img_path, specific_model):

    print("start infer_incision_plane")
    models = glob.glob(os.path.join(exp_path, "trained_models", specific_model + ".pth"))

    for model_index in range(len(models)):

        model_path = models[model_index]

        print("Check model: ", os.path.basename(model_path))
        cfg = get_cfg()
        cfg.MODEL.DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
        print(cfg.MODEL.DEVICE)
        cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"))



        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_first_labels())

        cfg.MODEL.WEIGHTS = model_path

        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8

        predictor = DefaultPredictor(cfg)

        os.makedirs("/dl_data/Expert_annotations/raw_images_AI/", exist_ok=True)

        try:
            im = cv2.imread(img_path)
        except BaseException:
            print("could not read image:", img_path)
            continue

        outputs = predictor(im)

        predicted_label = list(outputs['instances']._fields['pred_classes'].data.cpu().numpy())
        predicted_masks = list(outputs['instances']._fields['pred_masks'].data.cpu().numpy())

        # führe predicted labels zusammen,
        # falls diese zum gleichen label gehören

        predictions = {}
        for ii in range(len(predicted_masks)):

            if predicted_label[ii] in predictions:
                predictions[predicted_label[ii]] = np.logical_or(predicted_masks[ii],
                                                                     predictions[predicted_label[ii]])
            else:
                predictions[predicted_label[ii]] = predicted_masks[ii]

                img_pred = {}

                # plane

                if 0 in predictions:
                    img_pred[0] = predictions[0]

                # line
                #if 1 in predictions:
                #    img_pred[0] = predictions[1]

                create_rgb_img(label_mask=img_pred,
                               image_shape=np.shape(im),
                               segment_colours=[[128, 77, 0],[230, 0, 205]],
                               segment_label_value=1,
                               output_folder=os.path.join("/dl_data/Expert_annotations/raw_images_AI/", os.path.basename(img_path)))

def create_rgb_img(label_mask,image_shape,segment_colours,segment_label_value,output_folder):
    import PIL.Image as Image
    r = None

    for key, val in label_mask.items():
        segment_colour = segment_colours[key]

        if r is None:
            r = np.zeros((label_mask[key].shape[0], label_mask[key].shape[1]), dtype=np.float)  # label_mask.copy()
            g = r.copy()
            b = r.copy()

        r[label_mask[key] == segment_label_value] = np.float(segment_colour[0])  # label_colours[segment_name][0]
        g[label_mask[key] == segment_label_value] = np.float(segment_colour[1])  # label_colours[segment_name][1]
        b[label_mask[key] == segment_label_value] = np.float(segment_colour[2])

    if not r is None:
        rgb = np.zeros((image_shape[0], image_shape[1], 3))  # , dtype=np.uint8)
        rgb[:, :, 0] = r #* 255.0
        rgb[:, :, 1] = g #* 255.0
        rgb[:, :, 2] = b #* 255.0


        mask = Image.fromarray(rgb.astype(np.uint8), mode='RGB')
        mask.save(output_folder)

def inference2(exp_path, purpose, dataset, specific_model, vis_img=False):

    models = glob.glob(os.path.join(exp_path,"trained_models",specific_model+".pth"))

    global_average_jaccard = 0.0
    local_average_jaccard = 0.0
    final_model_path = None
    df_global = None
    if dataset == "pancreas":
        classes_list = ["pancreas_1", "pancreas_2", "pancreas_3", "stomach"]

    if dataset == "pancreas_binary":
        classes_list = ["pancreas", "pancreas"]

    if dataset == "VD":
        classes_list = ["inferior mesenteric artery", "inferior mesenteric vein", "plastic clip", "titan clip"]

    if dataset == "LM":
        classes_list = ["abdominal_wall","colon","adhesion","fat","small_intestine"]

    if dataset == "MM":
        classes_list = ["gerotas fascia","mesocolon","dissection line gerota","exploration area"]

    if dataset == "ME":
        classes_list = ["dissection plane TME", "dissection line TME", "rectum", "seminal vesicles"]

    if dataset == "first":
        classes_list = ["first incision", "plane first incision", "incised area", "instrument fenestrated bipolar forceps", "instrument permanent cautery hook", "instrument cadiere forceps","not needed"]


    if purpose == "train":
        for d in ["train"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))


    if purpose == "valid":
        for d in ["val"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))


    if purpose == "test":
        for d in ["test"]:
            DatasetCatalog.register(dataset + "_" + d, lambda d=d: load_dataset_dict(
                os.path.join(exp_path, "DATA", dataset + "_" + d + ".pkl")))

    if dataset == "pancreas":
        MetadataCatalog.get(dataset + "_" + d).set(
                    thing_colors=[(249, 179, 110), (209, 151, 93), (147, 105, 66), (216, 131, 105)])

    if dataset == "pancreas_binary":
        MetadataCatalog.get(dataset + "_" + d).set(
                    thing_colors=[(249, 179, 110), (249, 179, 110)])

    if dataset == "LM":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(170, 84, 68),  # abdominal wall
                          (204, 168, 142),  # colon
                          (110, 184, 209),  # adhesion
                          (230, 219, 70),  # fat
                          (255, 207, 177)  # small intensine
                          ])

    if dataset == "first":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(230, 0, 205),  # first incision
                          (128, 77, 0),  # mesocolon
                          (255, 255, 0),  # dissection line gerota
                          (0, 0, 126),
                          (96, 66, 54),
                          (84, 84, 126),
                          (170, 255, 255)
                          ])

    if dataset == "MM":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(151, 188, 207),
                          (128, 174, 128),
                          (219, 244, 20),
                          (255, 61, 193)
                          ])

    if dataset == "VD":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(216, 100, 79),
                          (0, 151, 205),
                          (184, 232, 61),
                          (230, 168, 28)
                          ])

    if dataset == "ME":
        MetadataCatalog.get(dataset + "_" + d).set(
            thing_colors=[(38, 121, 26),
                          (255, 0, 0),
                          (160, 130, 112),
                          (244, 172, 147)
                          ])

    for model_index in range(len(models)):

        local_average_jaccard = 0.0

        #model_path = os.path.join(exp_path,"trained_models","model_final.pth")
        model_path = models[model_index]

        print("Check model: ",os.path.basename(model_path))
        cfg = get_cfg()
        cfg.MODEL.DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
        cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml"))
        #train_name = "resection_train"
        #cfg.DATASETS.TRAIN = (train_name,)
        #cfg.DATASETS.TEST = ()
        #cfg.DATALOADER.NUM_WORKERS = NUM_WORKERS

        #cfg.SOLVER.IMS_PER_BATCH = 1
        #cfg.SOLVER.BASE_LR = LR
        #cfg.SOLVER.MAX_ITER = MAXITER
        #cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = ROI_HEADS_BATCH_SIZE_PER_IMAGE

        if dataset == "pancreas":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_pancreas_labels())

        if dataset == "LM":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_LM_labels())

        if dataset == "VD":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_VD_labels())

        if dataset == "ME":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_ME_labels())

        if dataset == "MM":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_MM_labels())

        if dataset == "first":
            cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(get_first_labels())


        #cfg.OUTPUT_DIR = os.path.join(exp_path,"prediction")


        cfg.MODEL.WEIGHTS = model_path

        # set the testing threshold for this model
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.8
        #cfg.DATASETS.TEST = ("pancreas_train",)

        predictor = DefaultPredictor(cfg)

        segment_jaccard = {}

        if dataset == "pancreas":
            label_desc = get_pancreas_label_desc()
            for ii in range(len(get_pancreas_labels())):
                segment_jaccard[ii] = []

        if dataset == "LM":
            label_desc = get_LM_label_desc()
            for ii in range(len(get_LM_labels())):
                segment_jaccard[ii] = []

        if dataset == "MM":
            label_desc = get_MM_label_desc()
            for ii in range(len(get_MM_labels())):
                segment_jaccard[ii] = []

        if dataset == "ME":
            label_desc = get_ME_label_desc()
            for ii in range(len(get_ME_labels())):
                segment_jaccard[ii] = []

        if dataset == "first":
            label_desc = get_first_label_desc()
            for ii in range(len(get_first_labels())):
                segment_jaccard[ii] = []

        if dataset == "VD":
            label_desc = get_VD_label_desc()
            for ii in range(len(get_VD_labels())):
                segment_jaccard[ii] = []

        #val_dict = get_pancreas_dicts(json_file="/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/pancreas_val.json")
        #val_dict = get_pancreas_dicts(
        #    json_file=os.path.join(exp_path,"DATA",))



        if purpose == "train":

            dataset_metadata = MetadataCatalog.get(dataset + "_train")

            data_dict = load_dataset_dict(os.path.join(exp_path,"DATA",dataset + "_train.pkl"))

        if purpose == "valid":

            dataset_metadata = MetadataCatalog.get(dataset + "_val")

            data_dict = load_dataset_dict(os.path.join(exp_path,"DATA",dataset + "_val.pkl"))

        if purpose == "test":

            dataset_metadata = MetadataCatalog.get(dataset + "_test")

            data_dict = load_dataset_dict(os.path.join(exp_path, "DATA", dataset + "_test.pkl"))


        os.makedirs( os.path.join(exp_path,"predictions",purpose),exist_ok=True)

        df = pd.DataFrame(columns=['model', 'label_id', 'label_name', 'metric', 'value', 'comment' ,'file_path'])


        label_number = 0
        for d in tqdm(data_dict):

            image_jaccard_index = {}

            #if not os.path.basename(d["file_name"]) == "frame_0098.png":
            #    continue

            label_name = d["file_name"].replace("frame", "label").replace("images","labels")

            try:
                #im = cv2.imread(d["file_name"])
                im = cv2.imread("/dl_data/Pancreas/20200330/images/frame_0061.png")
                #im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

                op_name = d["file_name"].split("/")[len(d["file_name"].split("/"))-3]

                if dataset == "pancreas_binary":
                    pass
                else:
                    #label = cv2.imread(label_name)
                    label = cv2.imread("/dl_data/Pancreas/20200330/labels/label_0061.png")
                    label = cv2.cvtColor(label, cv2.COLOR_BGR2RGB)

            except BaseException:
                print("could not read image/label", imgs[ii])
                continue

            #print(f'Filename = {d["file_name"]}')

            outputs = predictor(im)


            predicted_label = list(outputs['instances']._fields['pred_classes'].data.cpu().numpy())
            predicted_masks = list(outputs['instances']._fields['pred_masks'].data.cpu().numpy())
            predicted_boxes = list(outputs['instances']._fields['pred_boxes'].tensor.cpu().data.numpy())

            label_masks = get_binary_masks(mask=label, dataset=dataset)

            if len(label_masks) == 0:
                print("Label exists but is empty:", label_name)
                continue

            #if not len(label_masks) == len(predicted_label):
            #        print("Array length is different")
            #        continue


            # Label ist gegeben wurde aber nicht vorhergesagt
            for key, v in label_masks.items():
                if not key in predicted_label:
                    #print("Give label: ",key," was not found in predicted labels!")
                    jaccard_list = segment_jaccard[key]
                    jaccard_list.append(0.0)
                    segment_jaccard[key] = jaccard_list

                    image_jaccard_index[key] = 0.0
                    ternaus_jaccard = 0.0

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(key)] + [label_desc[key]] + ["ternaus_jaccard"] + [
                        str(ternaus_jaccard)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(key)] + [label_desc[key]] + ["jaccard_score"] + [
                                               str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(key)] + [label_desc[key]] + ["f1_score"] + [
                                               str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(key)] + [label_desc[key]] + ["precision_score"] + [
                                               str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

                    df.loc[label_number] = [os.path.basename(model_path)] + [str(key)] + [label_desc[key]] + ["recall_score"] + [
                                               str(0.0)] + ["Label was annotated but not predicted"] + [d["file_name"]]
                    label_number = label_number + 1

            predicted_label_ids = np.unique(predicted_label)

            # Wenn es weniger unique labels vorhanden sind als predicted_label dann muessen einige zusammengeführt werden
            if not len(predicted_label_ids) == len(predicted_label):

                predicted_masks_tmp = list.copy(predicted_masks)
                predicted_masks = []
                for jj in range(len(predicted_label_ids)):

                    predicted_mask_merge = np.zeros((label.shape[0], label.shape[1]), dtype=np.int16)
                    for kk in range(len(predicted_label)):
                        if predicted_label[kk] == predicted_label_ids[jj]:
                            #predicted_mask_to_binary = get_binary_masks(mask=predicted_masks_tmp[kk])

                            predicted_mask_merge = np.logical_or(predicted_masks_tmp[kk], predicted_mask_merge)

                    predicted_masks.append(predicted_mask_merge)


            for ii in range(len(predicted_label_ids)):

                jaccard_list = segment_jaccard[predicted_label_ids[ii]]

                if predicted_label_ids[ii] in label_masks:

                    ternaus_jaccard = jaccard_ternaus(
                        y_true=torch.from_numpy(label_masks[predicted_label_ids[ii]].astype(bool)).float(),
                        y_pred=torch.from_numpy(predicted_masks[ii]).float()).numpy().tolist()

                    jaccard_list.append(ternaus_jaccard)
                    image_jaccard_index[predicted_label_ids[ii]] = ternaus_jaccard

                    jaccard_score = metrics.jaccard_score(y_true=label_masks[predicted_label_ids[ii]].astype(bool).flatten(),
                                                   y_pred=predicted_masks[ii].astype(bool).flatten())

                    f1_score = metrics.f1_score(y_true=label_masks[predicted_label_ids[ii]].astype(bool).flatten(),
                                                   y_pred=predicted_masks[ii].astype(bool).flatten())

                    pre_score = metrics.precision_score(y_true=label_masks[predicted_label_ids[ii]].astype(bool).flatten(),
                                                   y_pred=predicted_masks[ii].astype(bool).flatten(), zero_division=0)

                    recall_score = metrics.recall_score(y_true=label_masks[predicted_label_ids[ii]].astype(bool).flatten(),
                                                   y_pred=predicted_masks[ii].astype(bool).flatten(), zero_division=0)

                else:
                    jaccard_list.append(0.0)
                    ternaus_jaccard = 0.0
                    jaccard_score = 0.0
                    f1_score = 0.0
                    pre_score = 0.0
                    recall_score = 0.0
                    image_jaccard_index[predicted_label_ids[ii]] = 0.0

                segment_jaccard[predicted_label_ids[ii]] = jaccard_list

                # add singe segment to df for statistc summary

                df.loc[label_number] = [os.path.basename(model_path)] + [str(predicted_label_ids[ii])] + [label_desc[predicted_label_ids[ii]]] + ["jaccard_score_ternaus"] + [
                    str(ternaus_jaccard)] + [""] + [d["file_name"]]
                label_number = label_number + 1

                df.loc[label_number] = [os.path.basename(model_path)] + [str(predicted_label_ids[ii])] + [label_desc[predicted_label_ids[ii]]] + ["jaccard_score"] + [
                    str(jaccard_score)] + [""] + [d["file_name"]]
                label_number = label_number + 1

                df.loc[label_number] = [os.path.basename(model_path)] + [str(predicted_label_ids[ii])] + [label_desc[predicted_label_ids[ii]]] + ["f1_score"] + [
                    str(f1_score)] + [""] + [d["file_name"]]
                label_number = label_number + 1

                df.loc[label_number] = [os.path.basename(model_path)] + [str(predicted_label_ids[ii])] + [label_desc[predicted_label_ids[ii]]] + ["precision_score"] + [
                    str(pre_score)] + [""] + [d["file_name"]]
                label_number = label_number + 1

                df.loc[label_number] = [os.path.basename(model_path)] + [str(predicted_label_ids[ii])] + [label_desc[predicted_label_ids[ii]]] + ["recall_score"] + [
                    str(recall_score)] + [""] + [d["file_name"]]
                label_number = label_number + 1

            if vis_img:
                v = Visualizer(im[:, :, ::-1], metadata=dataset_metadata, scale=0.75, instance_mode=ColorMode.SEGMENTATION)

                out = v.draw_instance_predictions(outputs["instances"].to("cpu"))
                #cv2.imwrite(
                #        os.path.join(exp_path,"predictions",purpose,op_name+"_"+os.path.basename(d["file_name"])),
                #        out.get_image()[:, :, ::-1])

                visualizer = Visualizer(im[:, :, ::-1], metadata=dataset_metadata, scale=0.75, instance_mode=ColorMode.SEGMENTATION)
                vis_org = visualizer.draw_dataset_dict(d)
                #cv2.imwrite(os.path.join(exp_path, "checkup", os.path.basename(d["file_name"])),
                #            vis_org.get_image()[:, :, ::-1])

                fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(18, 10))

                ax1.set_title('Annotated Image')
                ax1.imshow(vis_org.get_image())
                ax1.axes.get_yaxis().set_visible(False)
                ax1.axes.get_xaxis().set_visible(False)

                ax2.set_title('Predicted Image')
                ax2.imshow(out.get_image())
                ax2.axes.get_yaxis().set_visible(False)
                ax2.axes.get_xaxis().set_visible(False)

                str_single_jaccard = ""
                #for key, val in image_jaccard_index.items():
                #    str_single_jaccard += label_desc[key] +": J=" + str(image_jaccard_index[key]) + "; "

                from statistics import mean
                aj = mean(image_jaccard_index.values())
                str_single_jaccard += "AJ="+str(aj)
                fig.suptitle("OP: " + op_name + "\n Image: " + os.path.basename(d["file_name"]) + "\n " + str_single_jaccard )#+ "\n AJ:" + str(np.round(aj,3)))
                #plt.axis('off')
                plt.savefig(os.path.join(exp_path,"predictions",purpose,op_name+"_"+os.path.basename(d["file_name"])), dpi=300)
                plt.close('all')



        for key, val in segment_jaccard.items():
            local_average_jaccard += np.mean(val)

        print(local_average_jaccard/np.float(len(label_desc)))
        if (local_average_jaccard/np.float(len(label_desc))) > global_average_jaccard:
            print("Found new best model", os.path.basename(model_path), "with AJ:",local_average_jaccard/np.float(len(label_desc)))
            global_average_jaccard = local_average_jaccard/np.float(len(label_desc))
            final_model_path = model_path
            df_global = df.copy()

            #print(get_pancreas_label_desc()[key],": AJ=",np.mean(val))

    if not df_global == None:
        df_global.to_csv(os.path.join(exp_path,"performance","performance_"+purpose+".csv"))

    if purpose == "valid":
        DatasetCatalog.remove(dataset + "_val")
    else:
        DatasetCatalog.remove(dataset + "_" + purpose)

def main():
    arg_parser = ArgumentParser()
    arg_parser.add_argument("--exp-dir", "-ed", type=str, default="")
    arg_parser.add_argument("--modus", "-m", type=str, default="train_and_eval")
    #arg_parser.add_argument("--model-path", "-mp", type=str, default='/mnt/g27prist/TCO/TCO-Studenten/CoBot/Additional_segmentations_Fanny/pancreas/Experiments/LOOCV_004/trained_models/')
    #arg_parser.add_argument("--purpose", "-p", type=str, default='valid')
    arg_parser.add_argument("--specific-model", "-sm", type=str, default='*')
    arg_parser.add_argument("--export_images", "-ex", type=bool, default=False)
    #arg_parser.add_argument("--segment_mode", "-mode", type=str, default="binary")
    arg_parser.add_argument("--dataset", "-dataset", type=str, default="PD")


    args = arg_parser.parse_args()

    if args.modus == "train_and_eval":
        train_net(exp_path=args.exp_dir,
                  dataset=args.dataset)

        import pandas as pd

        specific_model = "*"
        purpose = "valid"

        inference(exp_path=args.exp_dir,
                  specific_model=specific_model,
                  purpose=purpose,
                  dataset=args.dataset,
                  vis_img=False)

        df = pd.read_csv(os.path.join(args.exp_dir, "performance", "performance_valid.csv"))
        specific_model = df['model'][1].replace(".pth", "")
        purpose = "test"

        inference(exp_path=args.exp_dir,
                  specific_model=specific_model,
                  purpose=purpose,
                  dataset=args.dataset,
                  vis_img=True)

        purpose = "valid"

        inference(exp_path=args.exp_dir,
                  specific_model=specific_model,
                  purpose=purpose,
                  dataset=args.dataset,
                  vis_img=True)

if __name__ == "__main__":
    main()
